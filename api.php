<?php
   header('Access-Control-Allow-Origin: *');
    error_reporting(E_ALL ^ E_NOTICE);
   // Define database connection parameters
   $hn      = 'localhost';
   $un      = 'root';            //username-of-database-here
   $pwd     = '';                //password-for-database-here
   $db      = 'mytickets'; //name-of-database
   $cs      = 'utf8';

   // Set up the PDO parameters
   $dsn  = 'mysql:host=' . $hn . ';port=3306;dbname=' . $db . ';charset=' . $cs;
   $opt  = array(
                        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
                        PDO::ATTR_EMULATE_PREPARES   => false,
                       );
   // Create a PDO instance (connect to the database)
   $pdo  = new PDO($dsn, $un, $pwd, $opt);
 
   // Retrieve specific parameter from supplied URL
   $key  = strip_tags($_REQUEST['key']);
   $data    = array();

   /* 
   print_r('<pre>');
   print_r($_SERVER);
   print_r('</pre>');
   */
   $sampleData ="[
 {
   eventid:'1',
   eventype:'Concierto', //Tipo
   eventname:'Noche de Salsa al Parque',  //Nombre
   eventdesc:' ',      //Descripcion
   eventorga:' ',      //organizador/promotor
   evetfepu :' ',      //Fecha de Publicacion
   eventfein:' ',      //FEcha Inicio
   eventfefi:' ',      //Fecha fin
   eventlike:3,        //Canidad de Likes
   eventdili:5,        //Cantidad de Dislike
   eventlati: 3.56789900,
   eventlong: -76.9878769,
   eventloca:' Cali - Parque Panamericano',
   localidades:[
                 {
				   locaid : 1,
				   locadesc: 'Gramilla',
				   locaprec : 50000,
				   locadispo : 45
				 },
				 {
				   locaid : 2,
				   locadesc: 'VIP',
				   locaprec : 150000,
				   locadispo : 60
				 }
    ],
    participantes: [
	             {
				   paevid : 1,
				   paevname : 'Gran Combo'
				 },
				 {
				   paevid : 2,
				   paevname : 'Telonero - Alquimia '				 
				 }
	
	
	],
    images : [ 
	           'http://midominio.com/myteickets/image/IMG98989898.png',
			   'http://midominio.com/myteickets/image/IMG98989896.png',
			   'http://midominio.com/myteickets/image/IMG98989897.png'
	          
	]	
 }
]   ";


//print_r($sampleData);   
 
   // Determine which mode is being requested
   switch($key)
   {
      // Add a new record into user table
      case 'register':

	    
          // Attempt to run PDO prepared statement
          try {
			$usuanomb=$_REQUEST["usuanomb"];
            $usuaapel=$_REQUEST["usuaapel"];
            $usuaemai=$_REQUEST["usuaemai"];
			$usuapass=$_REQUEST["usuapass"];
			$usuacedu=$_REQUEST["usuacedu"];
			$usuatele=$_REQUEST["usuatele"];
			$emprid_fk =1; //Empresa por defecto
			$usuaesta = 1; // Activo
			
            $token = str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789");
			$sql  = 'INSERT INTO usuarios(emprid_fk,usuanomb,usuaapel,usuaemai,usuapass,usuacedu,usuatele,usuaesta,usuatoke) VALUES(:emprid_fk,:usuanomb,:usuaapel,:usuaemai,:usuapass,:usuacedu,:usuatele,:usuaesta,:usuatoken)';
            $stmt    = $pdo->prepare($sql);
            $stmt->bindParam(':emprid_fk', $emprid_fk, PDO::PARAM_INT);
            $stmt->bindParam(':usuanomb', $usuanomb, PDO::PARAM_STR);
            $stmt->bindParam(':usuaapel', $usuaapel, PDO::PARAM_STR);
			$stmt->bindParam(':usuaemai', $usuaemai, PDO::PARAM_STR);
			$stmt->bindParam(':usuapass', $usuapass, PDO::PARAM_STR);
			$stmt->bindParam(':usuacedu', $usuacedu, PDO::PARAM_STR);
			$stmt->bindParam(':usuatele', $usuatele, PDO::PARAM_STR);
			$stmt->bindParam(':usuaesta', $usuaesta, PDO::PARAM_INT);
			$stmt->bindParam(':usuatoken',  $token, PDO::PARAM_INT);
			//print_r($stmt);
			
            if ($stmt->execute())
              echo json_encode(array('message' => 'Congratulations the record ' . $usuanomb . ' was added to the database','status' => 1));
	
          }
          // Catch any errors in running the prepared statement
          catch(PDOException $e)
          {
			 
			 echo json_encode(array('message' => 'Something went wrong! into server addeding record','status' => -1)); 
			 //echo '{status : -1,message:'Something went wrong! into server addeding record'}';
             echo $e->getMessage();
          }
      break;
	  case "auth":
           // Attempt to query database table and retrieve data
           try {  
		   
	            $email   = filter_var($_REQUEST['usuaemai'], FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
 	            $password   = filter_var($_REQUEST['usuapass'], FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);	
		   

                $stmt    = $pdo->query("SELECT * FROM usuarios where usuaemai= '".$email."' and usuapass=  '".$password."'");

                $stmt->execute();				
                while($row  = $stmt->fetch(PDO::FETCH_OBJ))
                { 
                   // Assign each row of data to associative array
                  $data[] = $row;
                }
                if(!$data)
				   //echo json_encode(array('id' => '0','email' => '_','password' => '_'));
                   echo json_encode(array(array('email' => $email,'password' => $password,'status' => -1,'message' => 'Usuario Invalido'))); 				
                // Return data as JSON
				else
				{
		  
                  echo json_encode(array('message' => 'Usuario Autenticado' . $usuanomb,'status' => 1));
				}  
            }
           catch(PDOException $e)
           {
			    echo json_encode(array('message' => 'Something went wrong! validating user','status' => -1)); 
                //echo $e->getMessage();
           }
   
	  break;	  
	  case "getAllEvents":
	  
 		   //$data = $sampleData;
   
           try {  
		   
		        $sql="
				      SELECT eventid,
					         tiposid_fk eventtype,
							 eventnomb,
							 eventcore eventdesc,
							 orgaid_fk eventpromo,
							 eventfepu,
							 eventfein,
							 eventfefi,
							 eventlike,
							 eventdili,
							 eventloca,
							 eventlati,
							 eventlong
					  FROM   eventos		 
					  ORDER BY eventfein		 
							 
					         
				
				";
                $stmt    = $pdo->query($sql);
				//print_r($stmt);
				$i=0;
				//while($row  = $stmt->fetch(PDO::FETCH_OBJ))
				foreach ($stmt as $row)
                
                { 
	                   // Assign each row of data to associative array
	                  $data[$i] = $row;

                      //Localidades
                      $localidades = array();
	                  $sql="SELECT * FROM localidades WHERE eventid_fk =".$row->eventid;
					  $stmt    = $pdo->query($sql);
					  //while($localidad  = $stmt->fetch(PDO::FETCH_OBJ))
					  foreach ($stmt as $localidad)
	                  { 
				        $localidades[] = $localidad;
				      }
					  
					  //Participantes
					  $participantes=array();
					  $sql="SELECT participantes.partid, participantes.partnomb
												FROM eventos,
													 participantes,
													 partevent
												WHERE eventos.eventid = partevent.eventid_fk
												AND   partevent.partid_fk = participantes.partid
												AND   eventos.eventid =".$row->eventid;
					  $stmt    = $pdo->query($sql);
					  //while($participante  = $stmt->fetch(PDO::FETCH_OBJ))
					  foreach ($stmt as $participante)
	                  { 
				        $participantes[] = $participante;
				      }				  
					  

                      //Imagenes
                      $imagenes=array();
					  $url = "http://".$_SERVER["SERVER_NAME"]."/myTickets/images/";
										  
                      $sql="SELECT            evimnomb 
											  FROM eventos, evenimag
											  WHERE  eventos.eventid = evenimag.eventid_fk
											  AND   eventos.eventid = ".$row->eventid;                      											  
				      //print_r($sql);						   
					  $stmt    = $pdo->query($sql);
					  
					  while($imagen  = $stmt->fetch(PDO::FETCH_OBJ))
					  //foreach ($stmt as $imagen)
	                  { 
					    
				        $imagenes[] = $url.$imagen->evimnomb;
				      }									
					  $data[$i]->images = $imagenes;
											  
			          $data[$i]->localidades = $localidades;
					  $data[$i]->participantes = $participantes;										  
					  $i++;
                }
				

                // Return data as JSON
				echo json_encode(array('status' => '1','message'=>'success','data' => $data));

            }
           catch(PDOException $e)
           {
			    echo json_encode(array('status' => '1','data' => $data,'message'=>'fail'));
                //echo $e->getMessage();
           }

        	   

      break;		
	  case 'getAllRecents' :
	  
 		   //Ultima Semana
		   
		   // Ultimo mes 
		   //WHERE eventfepu >= DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 2 MONTH)), INTERVAL 1 DAY)   
		   //AND   eventfepu <= DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 1 MONTH)), INTERVAL 0 DAY)
   
           try {  
		   
		        $sql="
				      SELECT eventid,
					         tiposid_fk eventtype,
							 eventnomb,
							 eventcore eventdesc,
							 orgaid_fk eventpromo,
							 eventfepu,
							 eventfein,
							 eventfefi,
							 eventlike,
							 eventdili,
							 eventloca,
							 eventlati,
							 eventlong
					  FROM   eventos	
                      WHERE eventfepu >= curdate() - INTERVAL DAYOFWEEK(curdate())+6 DAY
                      AND   eventfepu < curdate() - INTERVAL DAYOFWEEK(curdate())-1 DAY					  
					  ORDER BY eventfein		 
							 
					         
				
				";
                $stmt    = $pdo->query($sql);
				//print_r($stmt);
				$i=0;
                while($row  = $stmt->fetch(PDO::FETCH_OBJ))
                { 
                   // Assign each row of data to associative array
                  $data[$i] = $row;
				  $stmt    = $pdo->query("SELECT * FROM localidades WHERE eventid_fk =$row->eventid");
				  while($localidad  = $stmt->fetch(PDO::FETCH_OBJ))
                  { 
			        $localidades[] = $localidad;
			      }
				  
				  $stmt    = $pdo->query("SELECT participantes.partid, participantes.partnomb
											FROM eventos,
												 participantes,
												 partevent
											WHERE eventos.eventid = partevent.eventid_fk
											AND   partevent.partid_fk = participantes.partid
											AND   eventos.eventid = $row->eventid");
				  
				  while($participante  = $stmt->fetch(PDO::FETCH_OBJ))
                  { 
			        $participantes[] = $participante;
			      }				  
				  

				  $stmt    = $pdo->query("SELECT CONCAT('http://localhost/mytickets/imgs/',evenimag.evimnomb) evimnomb 
										  FROM eventos, evenimag
										  WHERE  eventos.eventid = evenimag.eventid_fk
										  AND   eventos.eventid = $row->eventid");
				  
				  while($imagen  = $stmt->fetch(PDO::FETCH_OBJ))
                  { 
			        $imagenes[] = $imagen->evimnomb;
			      }									
				  $data[$i]->images = $imagenes;
										  
		          $data[$i]->localidades = $localidades;
				  $data[$i]->participantes = $participantes;										  
				  $i++;
                }
				

                // Return data as JSON
				echo json_encode(array('status' => '1','message'=>'success','data' => $data));

            }
           catch(PDOException $e)
           {
			    echo json_encode(array('status' => '1','data' => $data,'message'=>'fail'));
                //echo $e->getMessage();
           }

        	   

      break;	  
	  case "likes" :
	  
 
           // Attempt to query database table and retrieve data
           try {  
                
				$eventid=$_REQUEST["eventid"];
				$sql="UPDATE eventos SET eventlike = eventlike + 1 WHERE eventid = :eventid";
				$stmt    = $pdo->prepare($sql);
                $stmt->bindParam(':eventid', $eventid, PDO::PARAM_INT);
                 			
                if ($stmt->execute())
                { 
                   
                  echo json_encode(array('status' => '1','message'=>'success'));
                }
                else
                // Return data as JSON
				 echo json_encode(array('status' => '1','data' => $data,'message'=>'success'));

            }
           catch(PDOException $e)
           {
			    echo json_encode(array('status' => '1','message'=>'fail'));
                //echo $e->getMessage();
           }

        	   

      break;
	  case "dislikes" :
	  
 
           // Attempt to query database table and retrieve data
           try {  
                
				$eventid=$_REQUEST["eventid"];
				$sql="UPDATE eventos SET eventdili = eventdili + 1 WHERE eventid = :eventid";
				$stmt    = $pdo->prepare($sql);
                $stmt->bindParam(':eventid', $eventid, PDO::PARAM_INT);
                 			
                if ($stmt->execute())
                { 
                   
                  echo json_encode(array('status' => '1','message'=>'success'));
                }
                else
                // Return data as JSON
				 echo json_encode(array('status' => '1','data' => $data,'message'=>'success'));

            }
           catch(PDOException $e)
           {
			    echo json_encode(array('status' => '1','message'=>'fail'));
                //echo $e->getMessage();
           }

        	   

      break;	  
	  
	  case "getAllTiposEventos":
	  
 		   //$data = $sampleData;
   
           try {  
		   
		        $sql="
				        SELECT tiposid,tiposdesc
						FROM tipos
						WHERE tipos.tipoid_fk = 1		 
							 
					         
				
				";
                $stmt    = $pdo->query($sql);
				//print_r($stmt);
				$i=0;
				//while($row  = $stmt->fetch(PDO::FETCH_OBJ))
				foreach ($stmt as $row)
                
                { 
	                   // Assign each row of data to associative array
	                  $data[] = $row;
					  

                }
				

                // Return data as JSON
				echo json_encode(array('status' => '1','message'=>'success','data' => $data));

            }
           catch(PDOException $e)
           {
			    echo json_encode(array('status' => '1','data' => $data,'message'=>'fail'));
                echo $e->getMessage();
           }
      break;	

	  case "getAllParticipantes":
	  
 		   //$data = $sampleData;
   
           try {  
		   
		        $sql="
				        SELECT partid,partnomb
						FROM participantes		 
							 
					         
				
				";
                $stmt    = $pdo->query($sql);
				//print_r($stmt);
				$i=0;
				//while($row  = $stmt->fetch(PDO::FETCH_OBJ))
				foreach ($stmt as $row)
                
                { 
	                   // Assign each row of data to associative array
	                  $data[] = $row;
					  

                }
				

                // Return data as JSON
				echo json_encode(array('status' => '1','message'=>'success','data' => $data));

            }
           catch(PDOException $e)
           {
			    echo json_encode(array('status' => '1','data' => $data,'message'=>'fail'));
                echo $e->getMessage();
           }
      break;

	  case "getAllEventsByTipo":
	  
 		   //$data = $sampleData;
           $tipoid=$_REQUEST["tipoid"];
           try {  
		   
		        $sql="
				      SELECT eventid,
					         tiposid_fk eventtype,
							 eventnomb,
							 eventcore eventdesc,
							 orgaid_fk eventpromo,
							 eventfepu,
							 eventfein,
							 eventfefi,
							 eventlike,
							 eventdili,
							 eventloca,
							 eventlati,
							 eventlong
					  FROM   eventos	
                      WHERE 	tiposid_fk = $tipoid				  
					  ORDER BY eventfein		 
							 
					         
				
				";
                $stmt    = $pdo->query($sql);
				//print_r($stmt);
				$i=0;
				//while($row  = $stmt->fetch(PDO::FETCH_OBJ))
				foreach ($stmt as $row)
                
                { 
	                   // Assign each row of data to associative array
	                  $data[$i] = $row;

                      //Localidades
                      $localidades = array();
	                  $sql="SELECT * FROM localidades WHERE eventid_fk =".$row->eventid;
					  $stmt    = $pdo->query($sql);
					  //while($localidad  = $stmt->fetch(PDO::FETCH_OBJ))
					  foreach ($stmt as $localidad)
	                  { 
				        $localidades[] = $localidad;
				      }
					  
					  //Participantes
					  $participantes=array();
					  $sql="SELECT participantes.partid, participantes.partnomb
												FROM eventos,
													 participantes,
													 partevent
												WHERE eventos.eventid = partevent.eventid_fk
												AND   partevent.partid_fk = participantes.partid
												AND   eventos.eventid =".$row->eventid;
					  $stmt    = $pdo->query($sql);
					  //while($participante  = $stmt->fetch(PDO::FETCH_OBJ))
					  foreach ($stmt as $participante)
	                  { 
				        $participantes[] = $participante;
				      }				  
					  

                      //Imagenes
                      $imagenes=array();
					  $url = "http://".$_SERVER["SERVER_NAME"]."/myTickets/images/";
										  
                      $sql="SELECT            evimnomb 
											  FROM eventos, evenimag
											  WHERE  eventos.eventid = evenimag.eventid_fk
											  AND   eventos.eventid = ".$row->eventid;                      											  
				      //print_r($sql);						   
					  $stmt    = $pdo->query($sql);
					  
					  while($imagen  = $stmt->fetch(PDO::FETCH_OBJ))
					  //foreach ($stmt as $imagen)
	                  { 
					    
				        $imagenes[] = $url.$imagen->evimnomb;
				      }									
					  $data[$i]->images = $imagenes;
											  
			          $data[$i]->localidades = $localidades;
					  $data[$i]->participantes = $participantes;										  
					  $i++;
                }
				

                // Return data as JSON
				echo json_encode(array('status' => '1','message'=>'success','data' => $data));

            }
           catch(PDOException $e)
           {
			    echo json_encode(array('status' => '1','data' => $data,'message'=>'fail'));
                //echo $e->getMessage();
           }

        	   

      break;	  
	  default :
      {	  
	   echo json_encode(array('message' => 'Fail !! ... No Method Exist, Retry','response' => -1,'status' => 1));   
	   
	   useApi();
	  } 	  
   }	

function useApi()
{
    print_r('<br><<<<<< Use Method for Usuario >>>>>><br>');
	print_r('Use for register User : <br> Example : .../Api.php?key=register&table=usuario&nombre=XXXX&email=XXXX&password=XXXXX<br><br>');
	print_r('Use for auth User : <br> Example : .../Api.php?key=auth&email=XXXX&password=XXXXX<br><br>');
	print_r('Use for Get Alls Vents : <br> Example : .../Api.php?key=geatAllEvents<br><br>');	
	print_r('Use for Get Recents Events : <br> Example : .../Api.php?key=getAllRecents<br><br>');
	print_r('Use for Get Eventos por Tipo : <br> Example : .../Api.php?key=getAllEventsByTipo&tipoid=XX<br><br>');
	print_r('Use for Get Todos los Participantes : <br> Example : .../Api.php?key=getAllParticipantes<br><br>');	
	print_r('Use for Get Todos los Tipos de Eventos : <br> Example : .../Api.php?key=getAllTiposEventos<br><br>');		
	
	
	
}    


?>

